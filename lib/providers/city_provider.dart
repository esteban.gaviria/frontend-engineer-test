import 'package:auth_firebase/models/city_models.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class CityProvider with ChangeNotifier {
  City _currentCity;
  Position _currentPosition;

  get currentCity {
    return _currentCity;
  }

  set currentCity(City city) {
    this._currentCity = city;
    notifyListeners();
  }

  get currentPosition {
    return _currentPosition;
  }

  set currentPosition(Position city) {
    this._currentPosition = city;
    notifyListeners();
  }
}
