import 'package:flutter/material.dart';

class FirebaseError extends StatelessWidget {
  const FirebaseError({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Title',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Error Connection'),
        ),
        body: Container(
          child: Center(
            child: Text('Error Connection...'),
          ),
        ),
      ),
    );
  }
}
