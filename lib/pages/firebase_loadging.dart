import 'package:flutter/material.dart';

class FirebaseLoading extends StatelessWidget {
  const FirebaseLoading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Title',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Loading Firebase'),
        ),
        body: Container(
          child: Center(
            child: Text('Loading Firebase'),
          ),
        ),
      ),
    );
  }
}
