class Restaurants {
  List<Restaurant> items = new List();

  Restaurants();

  Restaurants.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final restaurant = new Restaurant.fromJsonMap(item['restaurant']);
      if (restaurant.thumb != '' || items.length == 0) {
        items.add(restaurant);
      }
    }
  }
}

class Restaurant {
  String name;
  String thumb;

  Restaurant({
    this.name,
    this.thumb,
  });

  Restaurant.fromJsonMap(Map<String, dynamic> json) {
    name = json['name'];
    thumb = json['thumb'];
  }
}
