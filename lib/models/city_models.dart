class Cities {
  List<City> items = new List();

  Cities();

  Cities.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final city = new City.fromJsonMap(item);
      items.add(city);
    }
  }
}

class City {
  int id;
  String name;
  String countryFlagUrl;

  City({
    this.id,
    this.name,
    this.countryFlagUrl,
  });

  City.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    countryFlagUrl = json['country_flag_url'];
  }
}
