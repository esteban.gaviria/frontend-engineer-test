import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import 'package:auth_firebase/pages/firebase_loadging.dart';
import 'package:auth_firebase/pages/firebase_error.dart';
import 'package:auth_firebase/routes/main_route.dart';
import 'package:auth_firebase/routes/routes.dart';
import 'package:auth_firebase/routes/login_route.dart';
import 'package:auth_firebase/providers/city_provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(AppFirebase());
}

final FirebaseAuth _auth = FirebaseAuth.instance;

class AppFirebase extends StatelessWidget {
  // Create the initilization Future outside of `build`:
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return FirebaseError();
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MyApp();
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return FirebaseLoading();
      },
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => CityProvider()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Firebase Auth',
          home: _getLandingPage(),
          routes: getAppRoutes(),
        ));
  }

  // get landing page when open app
  Widget _getLandingPage() {
    return StreamBuilder<User>(
      stream: _auth.authStateChanges(),
      builder: (BuildContext context, user) {
        if (user.hasData) {
          if (user == null) {
            // logged in using email and password
            return LoginRoute();
          } else {
            // Main
            return MainRoute();
          }
        } else {
          // logged in using email and password
          return LoginRoute();
        }
      },
    );
  }
}
