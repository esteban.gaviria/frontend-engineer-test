import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class SignOut extends StatelessWidget {
  const SignOut({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (BuildContext context) {
      return FlatButton(
        child: const Text('Sign Out'),
        textColor: Theme.of(context).buttonColor,
        onPressed: () async {
          final user = _auth.currentUser;
          if (user == null) {
            Navigator.pushReplacementNamed(context, 'login');
            return;
          }
          await _auth.signOut();
          Navigator.pushReplacementNamed(context, 'login');
        },
      );
    });
  }
}
