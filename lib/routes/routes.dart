import 'package:auth_firebase/routes/main_route.dart';
import 'package:auth_firebase/routes/restaurant_route.dart';
import 'package:flutter/material.dart';

import 'package:auth_firebase/routes/login_route.dart';
import 'package:auth_firebase/routes/signup_route.dart';

Map<String, WidgetBuilder> getAppRoutes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => LoginRoute(),
    'signup': (BuildContext context) => SignUpRoute(),
    'main': (BuildContext context) => MainRoute(),
    'restaurants': (BuildContext context) => RestaurantsRoute(),
  };
}
