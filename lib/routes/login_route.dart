import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:auth_firebase/util/util.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  final GlobalKey<FormState> _loginForm = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _success;
  String _userEmail;
  String _errorMsg = '';

  @override
  Widget build(BuildContext context) {
    final String assetName = 'assets/svg/unlock.svg';
    final Widget svgHome =
        SvgPicture.asset(assetName, semanticsLabel: 'Movic Home');

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(30.0),
              height: 220.0,
              child: svgHome,
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              'Login',
              style: TextStyle(fontSize: 24),
            ),
            Form(
              key: _loginForm,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        labelText: 'Email', icon: Icon(Icons.email)),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Required Field';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      icon: Icon(Icons.lock),
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Required Field';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RaisedButton(
                        onPressed: () async {
                          if (_loginForm.currentState.validate()) {
                            _signInWithEmailAndPassword();
                          }
                        },
                        child: Text('Submit'),
                      ),
                      FlatButton(
                          onPressed: () {
                            Navigator.pushNamed(context, 'signup');
                          },
                          child: Text(
                            'Sign Up',
                            style: TextStyle(color: Colors.blue),
                          )),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      _success == null
                          ? ''
                          : (_success
                              ? 'Successfully signed in ' + _userEmail
                              : _errorMsg),
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _signInWithEmailAndPassword() async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: _emailController.text.trim(),
          password: _passwordController.text.trim());

      if (userCredential != null) {
        Navigator.pushReplacementNamed(context, 'main');
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Util.showAlert(context, e.code, 'No user found for that email');
      } else if (e.code == 'wrong-password') {
        Util.showAlert(
            context, e.code, 'Wrong password provided for that user.');
      } else {
        Util.showAlert(context, e.code, e.message);
      }
    } catch (e) {
      Util.showAlert(context, e.code, e.message);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
