import 'package:auth_firebase/util/util.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class SignUpRoute extends StatefulWidget {
  final String title = 'Registration';
  @override
  State<StatefulWidget> createState() => _SignUpRouteState();
}

class _SignUpRouteState extends State<SignUpRoute> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  // bool _success;
  // String _userEmail;

  @override
  Widget build(BuildContext context) {
    final String assetName = 'assets/svg/sign_in.svg';
    final Widget svgLogin =
        SvgPicture.asset(assetName, semanticsLabel: 'Movic Home');

    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(30.0),
              height: 220.0,
              child: svgLogin,
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              'Sign Up',
              style: TextStyle(fontSize: 24),
            ),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                        labelText: 'Email', icon: Icon(Icons.email)),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Required Field';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    obscureText: true,
                    controller: _passwordController,
                    decoration: const InputDecoration(
                        labelText: 'Password', icon: Icon(Icons.lock)),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Required Field';
                      }
                      return null;
                    },
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    alignment: Alignment.center,
                    child: RaisedButton(
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          _register(context);
                        }
                      },
                      child: const Text('Submit'),
                    ),
                  ),
                  // Container(
                  //   alignment: Alignment.center,
                  //   child: Text(_success == null
                  //       ? ''
                  //       : (_success
                  //           ? 'Successfully registered ' + _userEmail
                  //           : 'Registration failed')),
                  // )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _register(BuildContext context) async {
    try {
      UserCredential userCredential =
          await _auth.createUserWithEmailAndPassword(
              email: _emailController.text.trim(),
              password: _passwordController.text.trim());
      if (userCredential != null) {
        // Navigator.pushReplacementNamed(context, 'main');
        Navigator.of(context)
            .pushNamedAndRemoveUntil('main', (Route<dynamic> route) => false);
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Util.showAlert(context, e.code, 'The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        Util.showAlert(
            context, e.code, 'The account already exists for that email.');
      }
      Util.showAlert(context, e.code, e.message);
    } catch (e) {
      Util.showAlert(context, e.code, e.message);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
