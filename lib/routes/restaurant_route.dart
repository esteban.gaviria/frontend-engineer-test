import 'package:auth_firebase/models/city_models.dart';
import 'package:auth_firebase/providers/city_provider.dart';
import 'package:flutter/material.dart';

import 'package:auth_firebase/models/restaurant_model.dart';
import 'package:auth_firebase/services/restaurant_service.dart';
import 'package:auth_firebase/widgets/sign_out.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

class RestaurantsRoute extends StatefulWidget {
  RestaurantsRoute({Key key}) : super(key: key);

  @override
  _RestaurantsRouteState createState() => _RestaurantsRouteState();
}

class _RestaurantsRouteState extends State<RestaurantsRoute> {
  @override
  Widget build(BuildContext context) {
    final _cityProvider = Provider.of<CityProvider>(context);

    final City _currenCity = _cityProvider.currentCity;
    final Position _currenPosition = _cityProvider.currentPosition;

    if (_currenPosition != null) {}

    final latlon = _currenPosition != null
        ? 'lat: ${_currenPosition.latitude} / lon: ${_currenPosition.longitude}'
        : null;

    String title =
        _currenCity != null ? _currenCity.name : latlon != null ? latlon : '';

    return Container(
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[SignOut()],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
          child: Column(
            children: [
              Text(
                'Restaurantes',
                style: TextStyle(fontSize: 24, color: Colors.grey[800]),
              ),
              Text(title),
              SizedBox(
                height: 12.0,
              ),
              Expanded(
                child: _getRestaurants(context),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _getRestaurants(BuildContext context) {
    final restaurantService = new RestaurantService();

    return FutureBuilder(
      future: restaurantService.getRestautrants(context),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          print('snapshot.hasData!!!!!!!!!!!!');
          print(snapshot.data[0].name);
          return ListView(
            children: _getRestaurantListView(snapshot.data),
          );
        } else {
          return Container(
              height: 400.0, child: Center(child: CircularProgressIndicator()));
        }
      },
    );
  }

  List<Widget> _getRestaurantListView(List<Restaurant> items) {
    List<Widget> listItems = [];

    for (var i = 0; i < items.length; i++) {
      final card = Padding(
          padding: EdgeInsets.only(bottom: 8.0),
          child: _getRestaurantCard(items[i]));
      listItems.add(card);
    }

    return listItems;
  }

  Widget _getRestaurantCard(Restaurant restaurant) {
    var imageWidget;
    if (restaurant.thumb != null && restaurant.thumb != '') {
      // Thumb Image
      imageWidget = FadeInImage(
        height: 150.0,
        width: double.infinity,
        fit: BoxFit.cover,
        placeholder: AssetImage('assets/image/infinite-gif-preloader.gif'),
        image: NetworkImage(restaurant.thumb),
        fadeInDuration: Duration(milliseconds: 150),
      );
    } else {
      // Placeholder Image
      imageWidget = Image.asset(
        'assets/image/restaurant_placeholder.png',
        fit: BoxFit.cover,
        height: 150.0,
        width: double.infinity,
      );
    }

    return Card(
      elevation: 3.0,
      clipBehavior: Clip.antiAlias, // prevent overflow card
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          imageWidget,
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '${restaurant.name}',
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
