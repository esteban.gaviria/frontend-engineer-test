import 'package:auth_firebase/models/city_models.dart';
import 'package:auth_firebase/providers/city_provider.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:geolocator/geolocator.dart';

import 'package:auth_firebase/services/cities_service.dart';
import 'package:auth_firebase/widgets/sign_out.dart';
import 'package:provider/provider.dart';

class MainRoute extends StatefulWidget {
  MainRoute({Key key}) : super(key: key);

  @override
  _MainRouteState createState() => _MainRouteState();
}

class _MainRouteState extends State<MainRoute> {
  final citiesService = new CitiesService();
  final _searchQuery = new TextEditingController();
  Timer _debounce;
  CityProvider _cityProvider;

  @override
  Widget build(BuildContext context) {
    _cityProvider = Provider.of<CityProvider>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Ciudades'), actions: <Widget>[SignOut()]),
      body: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 24),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Donde desea comer:',
                style: TextStyle(fontSize: 24, color: Colors.grey[800]),
              ),
            ),
            SizedBox(height: 20),
            Padding(
                padding: EdgeInsets.only(bottom: 12), child: _createInput()),
            ListTile(
              leading: Icon(Icons.my_location),
              title: Text('Ubicacion actual'),
              onTap: () {
                _getCurrentLocation(context, true);
              },
            ),
            Divider(),
            Expanded(
              child: StreamBuilder(
                stream: citiesService.citiesStream,
                builder:
                    (BuildContext context, AsyncSnapshot<List<City>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView(
                      children: _createCityListView(snapshot.data, context),
                    );
                  } else {
                    return SizedBox();
                  }
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'restaurants');
                    },
                    child: Text('Restaurantes')),
                FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'restaurants');
                    },
                    child: Text('Historial'))
              ],
            )
          ],
        ),
      ),
    );
  }

  _getCurrentLocation(BuildContext context, bool navigate) async {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        print(position);
        if (navigate) {
          _cityProvider.currentPosition = position;
          _cityProvider.currentCity = null;

          _searchQuery.text = '';

          Navigator.pushNamed(context, 'restaurants');
        }
      });
    }).catchError((e) {
      print(e);
    });
  }

  Widget _createInput() {
    return TextField(
      autofocus: true,
      controller: _searchQuery,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        icon: Icon(Icons.location_city),
        hintText: 'New York',
        labelText: 'Ciudad',
      ),
    );
  }

  @override
  void initState() {
    print('initState');
    super.initState();
    _searchQuery.addListener(_onSearchChanged);
  }

  @override
  void dispose() {
    _searchQuery.removeListener(_onSearchChanged);
    _searchQuery.dispose();
    _debounce.cancel();
    _searchQuery.text = '';
    super.dispose();
  }

  _onSearchChanged() {
    print('_onSearchChanged');
    // debounce input text change
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      citiesService.getCities(_searchQuery.text);
    });
  }

  _createCityListView(List<City> cities, BuildContext context) {
    List<Widget> listItems = [];

    for (var i = 0; i < cities.length; i++) {
      Widget flag;

      if (cities[i].countryFlagUrl != null && cities[i].countryFlagUrl != '') {
        flag = Image.network(
          cities[i].countryFlagUrl,
          height: 20.0,
        );
      } else {
        flag = Icon(Icons.flag);
      }

      final listTile = Padding(
          padding: EdgeInsets.only(bottom: 8.0),
          child: Column(
            children: [
              ListTile(
                leading: flag,
                trailing: Icon(Icons.keyboard_arrow_right),
                title: Text(cities[i].name),
                onTap: () {
                  _cityProvider.currentPosition = null;
                  _cityProvider.currentCity = cities[i];
                  Navigator.pushNamed(context, 'restaurants');
                },
              ),
              Divider()
            ],
          ));
      listItems.add(listTile);
    }

    return listItems;
  }
}
