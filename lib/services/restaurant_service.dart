import 'package:auth_firebase/models/city_models.dart';
import 'package:auth_firebase/models/restaurant_model.dart';
import 'package:auth_firebase/providers/city_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';
import 'dart:async';

import 'package:provider/provider.dart';

class RestaurantService {
  String _apikey = 'db3f32e9155ba622ddb5567788343a75';
  String _url = 'developers.zomato.com';

  Map<String, String> get apiHeader => {
        "user-key": "$_apikey",
      };

  Future<List<Restaurant>> _procesarRespuesta(Uri url) async {
    print(url);
    final resp = await http.get(url, headers: apiHeader);
    final decodedData = json.decode(resp.body);

    final restaurants =
        new Restaurants.fromJsonList(decodedData['restaurants']);
    return restaurants.items;
  }

  Future<List<Restaurant>> getRestautrants(BuildContext context) async {
    final _cityProvider = Provider.of<CityProvider>(context);

    final City _currenCity = _cityProvider.currentCity;
    final Position _currenPosition = _cityProvider.currentPosition;

    var query = {'count': '20'};
    if (_currenCity != null) {
      query = {
        'entity_id': _currenCity.id.toString(),
        'entity_type': 'city',
        'radius': '200',
      };
    } else if (_currenPosition != null) {
      query = {
        'lat': _currenPosition.latitude.toString(),
        'lon': _currenPosition.longitude.toString(),
        'radius': '200',
      };
    }

    print(query);
    // final url = Uri.https(otraUrl, '');
    final url = Uri.https(_url, 'api/v2.1/search', query);
    return await _procesarRespuesta(url);
  }
}
