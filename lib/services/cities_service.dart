import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:auth_firebase/models/city_models.dart';

class CitiesService {
  String _apikey = 'db3f32e9155ba622ddb5567788343a75';
  String _url = 'developers.zomato.com';

  Map<String, String> get apiHeader => {
        "user-key": "$_apikey",
      };

  bool _cargando = false;

  List<City> _cities = new List();
  Function(List<City>) get citiesSink => _citiesStreamController.sink.add;

  final _citiesStreamController = StreamController<List<City>>.broadcast();
  Stream<List<City>> get citiesStream => _citiesStreamController.stream;

  void disposeStreams() {
    _citiesStreamController?.close();
  }

  Future<List<City>> _procesarRespuesta(Uri url) async {
    final resp = await http.get(url, headers: apiHeader);
    final decodedData = json.decode(resp.body);

    final cities = new Cities.fromJsonList(decodedData['location_suggestions']);
    return cities.items;
  }

  Future<List<City>> getCities(String query) async {
    if (_cargando) return [];

    _cities = [];
    if (query.length <= 1) {
      citiesSink(_cities);
      return [];
    }

    _cargando = true;

    final url = Uri.https(_url, 'api/v2.1/cities', {'q': query});
    final resp = await _procesarRespuesta(url);

    _cities.addAll(resp);
    citiesSink(_cities);

    _cargando = false;
    return resp;
  }
}
