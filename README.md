# flutter-frontend-engineer-test

### Inicio
Esta es una aplicación de flutter creada a partir del comando [Flutter: New Project](https://flutter.dev/docs/get-started/test-drive?tab=vscode/), haciendo uso de firebase para la gestión  de usuarios (autenticación y autorización), e integrando las APIs proporcionadas por [zoomato](https://developers.zomato.com/ ) para realizar la búsqueda y visualización de restaurantes.

- [Documentación oficial de flutter](https://flutter.dev/)

### Firebase

Este repositorio usa Firebase. Usando Firebase Authentication para hacer el login y registro de usuarios, y validar el estado actual de la authenticación para iniciar la aplicación y mostrar la pantalla correspondiente.

- [Firebase Flutter Docs](https://firebase.flutter.dev/docs/overview)


### Zomato API
Se hace uso de Las APIs de Zomato  que permiten acceder a información actualizada de más de 1.5 millones de restaurantes en 10,000 ciudades en todo el mundo.

- [Zoomaro Documentation](https://developers.zomato.com/documentation)

### Dependencies
```
  cupertino_icons: ^0.1.3
  firebase_auth: ^0.18.0+1
  firebase_core: ^0.5.0
  provider: ^4.3.2+1
  geolocator: ^5.3.2+2
```

## Creador

**Esteban Gaviria Restrepo**


## Gracias

¡Muchas gracias a todos, si se desea continuar con este proyecto estaré pendiente!

## Copyright and license

Código  generado bajo la licencia [MIT License](https://es.wikipedia.org/wiki/Licencia_MIT).

Enjoy!!